$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "consecpol/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "consecpol"
  s.version     = Consecpol::VERSION
  s.authors     = ["Mark Hoad"]
  s.email       = ["markdhoad@gmail.com"]
  s.homepage    = "https://gitlab.com/mhoad/ConSecPol"
  s.summary     = "Simple Rails engine for managing Content Security Policy violation reports."
  s.description = "Drop in engine that provides a CSP violation reporting endpoint."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.2.0"

  s.add_development_dependency "sqlite3"
end
